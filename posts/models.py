from django.db import models

class Post(models.Model):
    title = models.CharField(max_length=100)
    content = models.TextField()
    created = models.DateTimeField(null = True)

    def __str__(self):
        return f"{self.title}"

class Comment(models.Model):
    author = models.CharField(max_length=100)
    created = models.DateTimeField(auto_now_add=True, null=True)
    content = models.TextField(max_length=1000)
    post = models.ForeignKey("Post", related_name="comments", on_delete=models.PROTECT
    )

    def __str__(self):
        return f"Comment By {self.author} on {self.post}"

class Keyword(models.Model):
    word = models.CharField(max_length=20)
    posts = models.ManyToManyField("Post", related_name="keywords")

    def __str__(self):
        return self.word
